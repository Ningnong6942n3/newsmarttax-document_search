*** Settings ***
Library     Selenium2Library
Resource    functions/Login.robot
*** Variables ***
${url}  https://newsmarttax.brainergy.digital/login
${browser}  chrome
${DocumentSearch}  https://newsmarttax.brainergy.digital/search

*** Keywords ***


Check Error Message
    Wait Until Page Contains  Unable to log in with provided credentials.  10s
    ${ErrorMessage}  Get Text  //div[@class='col-sm-8 col-lg-4 offset-lg-4 px-lg-5']/div[@class='text-red text-center my-1 d-block']
    Should Contain  Unable to log in with provided credentials.  ${ErrorMessage}
    Close Browser

*** Test Cases ***
Test Login - Input Username And Password Wrong
    Open Website Newsmarttax
    Input Username And Password Wrong
    Click Btn Login (False)
    Check Error Message
