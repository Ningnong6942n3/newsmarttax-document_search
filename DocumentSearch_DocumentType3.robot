*** Settings ***
Library     Selenium2Library
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/DocumentSearch.robot
*** Variables ***
${DocumentType}  Invoice/Tax Invoice
*** Keywords ***




Check Accuracy DocumentType
     Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div  20s
     ${ww}  Run Keyword And Return Status  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/div/h5
     Run Keyword If  '${ww}' == 'True'  Check Accuracy 
     Run Keyword If  '${ww}' != 'True'  Check Accuracy DocumentType1
Check Accuracy 
    ${ss}  Get Text  //div[@class='box-parent position-relative z-1']/div/div/h5
    Should Contain  ${ss}  Data not Found
    Close browser
Check Accuracy DocumentType1
    Wait Until Element Is Visible  //div[@class='dv-pagination']/ngb-pagination/ul/li  10s
    ${count} =	Get Element Count  //div[@class='dv-pagination']/ngb-pagination/ul/li
    IF   '${count}' == '3'  
           ${countSh} =	Get Element Count  //tbody[@class='ui-table-tbody']/tr
           FOR    ${ni}    IN RANGE    ${countSh}
                  ${i}  Evaluate  ${ni}+1
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[${i}]/td[5]
                  ${DocumentTypeTest}  Get Text  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[${i}]/td[5]
                  Should Contain  ${DocumentType}   ${DocumentTypeTest} 
                  
           END 
     ELSE
           ${n}  Evaluate  ${count}-2
           Log to Console  ${n} 
           FOR    ${i}    IN RANGE    ${n}
                  ${ns}  Evaluate  ${i}+1
                  Wait Until Element Is Visible  //div[@class='dv-pagination']/ngb-pagination/ul/li  10s
                  Click Element  //div[@class='dv-pagination']/ngb-pagination/ul/li/a[text()=${ns}]
                  Sleep  2
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
                  ${countSh} =  Get Element Count    //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  
                  Log to Console  ${countSh}
                  FOR    ${ni}    IN RANGE    ${countSh}
                  ${i}  Evaluate  ${ni}+1
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[${i}]/td[5]
                  ${DocumentTypeTest}  Get Text  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[${i}]/td[5]
                  Should Contain  ${DocumentType}   ${DocumentTypeTest} 
                  END
                  
           END   
           
     END
     Close browser
*** Test Cases ***
Test Filter-DocumentType3
    Open Website Newsmarttax
    Input Username And Password (Accountant)
    Click Btn Login
    Click Drop Down DocumentType3
    Check Accuracy DocumentType
   