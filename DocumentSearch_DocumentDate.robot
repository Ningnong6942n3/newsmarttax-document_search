*** Settings ***

Library     Selenium2Library
Library     BuiltIn
Library     String
Resource    functions/Login.robot
Resource    functions/DocumentSearch.robot

*** Variables ***
${DateCh}  01/07/
*** Keywords ***


Check Accuracy Document
     Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div  20s
     Sleep  2
     ${ww}  Run Keyword And Return Status  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/div/h5
     Run Keyword If  '${ww}' == 'True'  Check Accuracy 
     Run Keyword If  '${ww}' != 'True'  Check Accuracy DocumentType1
Check Accuracy 
    ${ss}  Get Text  //div[@class='box-parent position-relative z-1']/div/div/h5
    Should Contain  ${ss}  Data not Found
    Close Browser
Check Accuracy DocumentType1
    Wait Until Element Is Visible  //div[@class='dv-pagination']/ngb-pagination/ul/li  10s
    Sleep  5
    ${count} =	Get Element Count  //div[@class='dv-pagination']/ngb-pagination/ul/li
    IF   '${count}' == '3'
           ${DataTime}  Get Text  //tbody[@class='ui-table-tbody']/tr/td[3]
           ${Do}  Fetch From Left  ${DataTime}  2021
           Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
           ${countSh} =  Get Element Count    //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  
           Log to Console  ${countSh}
            FOR    ${ni}    IN RANGE    ${countSh}
                  ${i}  Evaluate  ${ni}+1
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[${i}]/td[3]
                  ${DataTime}  Get Text  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[${i}]/td[3]
                  ${Do}  Fetch From Left  ${DataTime}  2021
                  Log To Console  ${Do} 
                  Should Contain  ${DateCh}   ${Do}
                  
            END
     ELSE
           ${n}  Evaluate  ${count}-2
           Log to Console  ${n} 
           FOR    ${i}    IN RANGE    ${n}
                  ${ns}  Evaluate  ${i}+1
                  Wait Until Element Is Visible  //div[@class='dv-pagination']/ngb-pagination/ul/li  10s
                  Click Element  //div[@class='dv-pagination']/ngb-pagination/ul/li/a[text()=${ns}]
                  Sleep  2
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
                  ${countSh} =  Get Element Count    //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  
                  Log to Console  ${countSh}
                  FOR    ${ni}    IN RANGE    ${countSh}
                  ${i}  Evaluate  ${ni}+1
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[${i}]/td[3]
                  ${DataTime}  Get Text  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[${i}]/td[3]
                  ${Do}  Fetch From Left  ${DataTime}  2021
                  Log To Console  ${Do} 
                  Should Contain  ${DateCh}   ${Do}
                  END
                  
           END   
           
     END 
     Close Browser


*** Test Cases ***
Test Filter-Document Date
    Open Website Newsmarttax
    Input Username And Password (Accountant)
    Click Btn Login
    Choose Document Date-Since
    Choose Document Date-Until
    Check Accuracy Document
    
  
    