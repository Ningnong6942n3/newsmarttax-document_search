*** Settings ***
Library     BuiltIn
*** Variables ***


*** Keywords ***

Click Drop Down Branch1
    Sleep  2
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-1 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']  10s
    Click Element  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-1 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']
    Wait Until Element Is Visible  //div[@class='ui-dropdown-items-wrapper']/ul/li[2]  10s
    ${Branch}  Get Text  //div[@class='ui-dropdown-items-wrapper']/ul/li[2]
    set global variable  ${Branch}
    Log to Console  ${Branch}
    Click Element  //div[@class='ui-dropdown-items-wrapper']/ul/li[2]
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']

Click Drop Down Branch2
    Sleep  2
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-1 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']  10s
    Click Element  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-1 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']
    Wait Until Element Is Visible  //div[@class='ui-dropdown-items-wrapper']/ul/li[3]  10s
    ${Branch}  Get Text  //div[@class='ui-dropdown-items-wrapper']/ul/li[3]
    set global variable  ${Branch}
    Log to Console  ${Branch}
    Click Element  //div[@class='ui-dropdown-items-wrapper']/ul/li[3]
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']
    
Click Drop Down Company (BNG)
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/p-dropdown/div/label[@class='ng-tns-c6-0 ui-dropdown-label ui-inputtext ui-corner-all ng-star-inserted']  10s
    Click Element  //div[@class='col-md-4']/div/p-dropdown/div/label[@class='ng-tns-c6-0 ui-dropdown-label ui-inputtext ui-corner-all ng-star-inserted']
    Wait Until Element Is Visible  //div[@class='ng-trigger ng-trigger-overlayAnimation ng-tns-c6-0 ui-dropdown-panel ui-widget ui-widget-content ui-corner-all ui-shadow ng-star-inserted']/div/ul/li[@class='ng-tns-c6-0 ui-dropdown-item ui-corner-all ng-star-inserted']/span[text()='บริษัท เบรนเนอร์จี้ จำกัด']  10s
    Click element  //div[@class='ng-trigger ng-trigger-overlayAnimation ng-tns-c6-0 ui-dropdown-panel ui-widget ui-widget-content ui-corner-all ui-shadow ng-star-inserted']/div/ul/li[@class='ng-tns-c6-0 ui-dropdown-item ui-corner-all ng-star-inserted']/span[text()='บริษัท เบรนเนอร์จี้ จำกัด']
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[@class='btn btn-dv']  10s
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']  
    

Click Drop Down DocumentType1
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']  10s
    Click Element  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']
    Wait Until Element Is Visible  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบกำกับภาษี (Tax Invoice)']  10s
    Click Element  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบกำกับภาษี (Tax Invoice)']
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[@class='btn btn-dv']  10s
    Sleep  2
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
 

Click Drop Down DocumentType2
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']  10s
    Click Element  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']
    Wait Until Element Is Visible  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบเสร็จรับเงิน/ใบกำกับภาษี (Receipt/Tax Invoice)']  10s
    Click Element  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบเสร็จรับเงิน/ใบกำกับภาษี (Receipt/Tax Invoice)']
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[@class='btn btn-dv']  10s
    Sleep  2
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s  

Click Drop Down DocumentType3
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']  10s
    Click Element  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']
    Wait Until Element Is Visible  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบแจ้งหนี้/ใบกำกับภาษี (Invoice/Tax Invoice)']  10s
    Click Element  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบแจ้งหนี้/ใบกำกับภาษี (Invoice/Tax Invoice)']
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[@class='btn btn-dv']  10s
    Sleep  2
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s  
    
Click Drop Down DocumentType4
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']  10s
    Click Element  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']
    Wait Until Element Is Visible  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบเสร็จรับเงิน (Receipt)']  10s
    Click Element  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบเสร็จรับเงิน (Receipt)']
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[@class='btn btn-dv']  10s
    Sleep  2
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s  

Click Drop Down DocumentType5
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']  10s
    Click Element  //div[@class='col-md-4']/div/p-dropdown/div[@class='ng-tns-c6-2 ui-dropdown ui-widget ui-state-default ui-corner-all ui-helper-clearfix']
    Wait Until Element Is Visible  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบแจ้งหนี้ (Invoice)']  10s
    Click Element  //div[@class='ui-dropdown-items-wrapper']/ul/li/span[text()='ใบแจ้งหนี้ (Invoice)']
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[@class='btn btn-dv']  10s
    Sleep  2
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s  
    
Input Document Number
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[1]/td[2]  20s 
    ${DocumentName}  Get Text  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr[1]/td[2]
    Input Text  //div[@class='col']/div/input  ${DocumentName}
    set global variable  ${DocumentName}
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[@class='btn btn-dv']  20s 
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s  

Choose Document Date-Since
    Wait Until Element Is Visible  //div[@class='col-md-4']/div/div[@class='form-label-group']  10s
    Click Element  //div[@class='col-md-4']/div/div[@class='form-label-group']
    Wait Until Element Is Visible  //div[@class='ui-datepicker-group ui-widget-content ng-tns-c7-3 ng-star-inserted']/div/div/select/option[7]  10s
    ClicK Element  //div[@class='ui-datepicker-group ui-widget-content ng-tns-c7-3 ng-star-inserted']/div/div/select/option[7]
    Wait Until Element Is Visible  //tbody[@class='ng-tns-c7-3']/tr/td/a[text()=1]  10s
    Click Element  //tbody[@class='ng-tns-c7-3']/tr/td/a[text()=1]

Choose Document Date-Until
    Wait Until Element Is Visible  //p-calendar[2]/span/div/div/div[@class='ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all']/div/select/option[7]  10s
    Click Element  //p-calendar[2]/span/div/div/div[@class='ui-datepicker-header ui-widget-header ui-helper-clearfix ui-corner-all']/div/select/option[7]
    Wait Until Element Is Visible  //tbody[@class='ng-tns-c7-4']/tr/td/a[text()=1]  10s
    Click Element  //tbody[@class='ng-tns-c7-4']/tr/td/a[text()=1]
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[@class='btn btn-dv']  10s
    Sleep  2
    Click Button  //div[@class='col-auto']/div/button[@class='btn btn-dv']
    
    