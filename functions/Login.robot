*** Variables ***

${url}  https://newsmarttax.brainergy.digital/login
${browser}  chrome
${DocumentSearch}  https://newsmarttax.brainergy.digital/search

*** Keywords ***

Open Website Newsmarttax
    Open Browser  ${url}  ${browser}
    wait until location is  ${url}  10s
    Maximize Browser Window

Input Username And Password (Accountant)
    Wait Until Element Is Visible  name=username  10s
    Input Text  name=username  bngacc
    Wait Until Element Is Visible  name=password  10s
    Input Text  name=password  Bng12345!
    
Click Btn Login
    Click Button  //div[@class='form-group']/button
    wait until location is  ${DocumentSearch}  10s

Click Btn Login (False)
    Click Button  //div[@class='form-group']/button
    Location Should Be  ${url}

Input Username Only
    Input Text  name=username  bngacc

Input Password Only
    Input Text  name=password  Bng12345!

Input Password Only Wrong
    Input Text  name=password  Bng12345

Input Username Only Wrong
    Input Text  name=username  bngac

Input Username And Password Wrong
    Input Text  name=username  bngac
    Input Text  name=password  Bng12345