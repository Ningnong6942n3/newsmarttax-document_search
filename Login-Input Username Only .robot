*** Settings ***

Library     Selenium2Library
Resource    functions/Login.robot
*** Variables ***


*** Keywords ***


Check Error Message
    Wait Until Page Contains  Please complete all information.  10s
    ${ErrorMessage}  Get Text  //div[@class='col-sm-8 col-lg-4 offset-lg-4 px-lg-5']/div[@class='text-red text-center my-1 d-block']
    Should Contain  Please complete all information.  ${ErrorMessage}
    Close Browser

*** Test Cases ***
Test Login - Input Username Only
    Open Website Newsmarttax
    Input Username Only
    Click Btn Login (False)
    Check Error Message
