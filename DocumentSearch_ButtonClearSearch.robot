*** Settings ***
Library     Selenium2Library
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/DocumentSearch.robot

*** Variables ***


*** Keywords ***
Check Accuracy DocumentName
     Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div  20s
     ${ww}  Run Keyword And Return Status  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/div/h5
     Run Keyword If  '${ww}' == 'True'  Check Accuracy 
     Run Keyword If  '${ww}' != 'True'  Check Accuracy DocumentType1
Check Accuracy 
    ${ss}  Get Text  //div[@class='box-parent position-relative z-1']/div/div/h5
    Should Contain  ${ss}  Data not Found
Check Accuracy DocumentType1
    Wait Until Element Is Visible  //div[@class='dv-pagination']/ngb-pagination/ul/li  10s
    Wait Until Element Is Visible  //tbody[@class='ui-table-tbody']/tr/td[2]  10s
    ${DocumentNumberTest}  Get Text  //tbody[@class='ui-table-tbody']/tr/td[2]
    Should Contain  ${DocumentName}  ${DocumentNumberTest}

Click btn Clear Search
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
    Wait Until Element Is Visible  //div[@class='col-auto']/div/button[1]  20s
    Click Button  //div[@class='col-auto']/div/button[1]


Check Clear Search
    Wait Until Element Is Visible  //div[@class='dv-pagination']/ngb-pagination/ul/li  10s
    ${count} =	Get Element Count  //div[@class='dv-pagination']/ngb-pagination/ul/li
    Log to Console  ${count} 
    IF   '${count}' == '3'
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
                  ${countSh} =  Get Element Count    //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  
                  Log to Console  ${countSh}
                  
    ELSE 
           ${n}  Evaluate  ${count}-2
           Log to Console  ${n} 
           FOR    ${i}    IN RANGE    ${n}
                  ${ns}  Evaluate  ${i}+1
                  Wait Until Element Is Visible  //div[@class='dv-pagination']/ngb-pagination/ul/li  10s
                  Click Element  //div[@class='dv-pagination']/ngb-pagination/ul/li/a[text()=${ns}]
                  Sleep  2
                  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
                  ${countSh} =  Get Element Count    //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  
                  Log to Console  ${countSh}
                  
           END  
           
     END
     Close Browser



    



*** Test Cases ***
Test Filter-Document Number
    Open Website Newsmarttax
    Input Username And Password (Accountant)
    Click Btn Login
    Input Document Number
    Check Accuracy DocumentName
    Click btn Clear Search
    Check Clear Search
   
