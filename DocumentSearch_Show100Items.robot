*** Settings ***
Library     Selenium2Library
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/DocumentSearch.robot
*** Variables ***



*** Keywords ***

Choose Show 40 Items/Pages
    Wait Until Element Is Visible  //div[@class='col-auto page-size ng-star-inserted']/p-dropdown/div/label  10s
    Click Element  //div[@class='col-auto page-size ng-star-inserted']/p-dropdown/div/label
    Wait Until Element Is Visible  //div[@class='ui-dropdown-items-wrapper']/ul/li[2]  10s
    Click Element  //div[@class='ui-dropdown-items-wrapper']/ul/li[2]
Check Show 40 Items/Pages
    Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  20s
    Wait Until Element Is Visible  //div[@class='col-auto page-size ng-star-inserted']/p-dropdown/div/label  20s
    ${countSh} =  Get Element Count    //div[@class='box-parent position-relative z-1']/div/p-table/div/div/table/tbody/tr  
    Log to Console  ${countSh}
    Should Contain  '${countSh}'  '100'
    Close Browser
*** Test Cases ***
Test Filter-Branch 
    Open Website Newsmarttax
    Input Username And Password (Accountant)
    Click Btn Login
    Choose Show 40 Items/Pages
    Check Show 40 Items/Pages
    
    
