*** Settings ***
Library     Selenium2Library
Library     BuiltIn
Resource    functions/Login.robot
Resource    functions/DocumentSearch.robot

*** Variables ***


*** Keywords ***
Check Accuracy DocumentName
     Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div  20s
     ${ww}  Run Keyword And Return Status  Wait Until Element Is Visible  //div[@class='box-parent position-relative z-1']/div/div/h5
     Run Keyword If  '${ww}' == 'True'  Check Accuracy 
     Run Keyword If  '${ww}' != 'True'  Check Accuracy DocumentType1
Check Accuracy 
    ${ss}  Get Text  //div[@class='box-parent position-relative z-1']/div/div/h5
    Should Contain  ${ss}  Data not Found
Check Accuracy DocumentType1
    Wait Until Element Is Visible  //div[@class='dv-pagination']/ngb-pagination/ul/li  10s
    Wait Until Element Is Visible  //tbody[@class='ui-table-tbody']/tr/td[2]  10s
    ${DocumentNumberTest}  Get Text  //tbody[@class='ui-table-tbody']/tr/td[2]
    Should Contain  ${DocumentName}  ${DocumentNumberTest}
    Close Browser




    



*** Test Cases ***
Test Filter-Document Number
    Open Website Newsmarttax
    Input Username And Password (Accountant)
    Click Btn Login
    Input Document Number
    Check Accuracy DocumentName
   
